﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleMeteor : MonoBehaviour
{
    public float speed = 10;
    public float force = 500;

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        rb.AddForce(force * Random.onUnitSphere);
    }

    void FixedUpdate()
    {
        rb.velocity = rb.velocity.normalized * speed;
    }
}
