﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleShip : MonoBehaviour
{
    public GameObject explosion;

    public GameObject laser;

    public float coolDown = .25f;

    public float rotationSpeed = 5;
    public float boostSpeed = 50;

    private Rigidbody2D rb;
    private float nextShot = 0;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        rb.AddTorque(-Input.GetAxis("Horizontal") * rotationSpeed);

        float boost = Input.GetAxis("Vertical");

        if (boost > 0)
        {
            rb.AddForce(boost * boostSpeed * transform.up);
        }

        if (Input.GetButton("Fire1") && Time.time > nextShot)
        {
            nextShot = Time.time + coolDown;

            GameObject go =
                Instantiate(
                    laser,
                    transform.position,
                    Quaternion.identity
                );

            go.transform.up = transform.up;
            go.GetComponent<Rigidbody2D>().velocity = transform.up * 10;

            Destroy(go, 2);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Walls"))
        {
            GameObject go =
                Instantiate(
                    explosion,
                    transform.position,
                    explosion.transform.rotation);

            Destroy(go, 2);

            Destroy(gameObject);
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Walls"))
        {
            rb.angularVelocity = 0;

            Vector2 dir = rb.velocity.normalized;

            transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90);
        }
    }
}
