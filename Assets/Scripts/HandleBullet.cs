﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleBullet : MonoBehaviour
{
    public GameObject explosion;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Asteroids"))
        {
            GameObject go = Instantiate(
                explosion,
                transform.position,
                explosion.transform.rotation);

            Destroy(go, 1);

            Destroy(other.gameObject);

            Destroy(gameObject);
        }
    }
}
